#!/usr/bin/env python3
#import sys, os, math, glob
#
# run with 
#
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
user_args = sys.argv[1:]
for filename in sys.argv[1:]:
    arr = filename.split('.')
    solver_type = arr[0]
    print (solver_type)
    J = []
    J_re = []
    n_e = []
    T_e = []
    time = []
    E = []
    Jidx = -1
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if n > 12 and words[0] == 'testSpitzer:':
            #  0              1  2     3          4     5         6   7         8   9         0      1         2 3 4        5         6 7      8   9  0   1             2   3      4           5 
            # testSpitzer:    1) time= 1.0000e-01 n_e=  1.000e+00 E=  5.099e-02 J=  2.544e+06 J_re=  0.000e+00 0 % Te_kev=  9.987e+00 Z_eff=1. E/J to eta ratio=37.824 (diff=4.94365e+14) vz= 0.00378455 constant E normal
            tm = float(words[3])
            time.append(tm)
            if Jidx == -1 and tm > 25: Jidx = len(time) - 1
            tm = float(words[5])
            n_e.append(tm)
            tm = float(words[7])
            E.append(tm)
            tm = float(words[9])
            J.append(tm)
            tm = float(words[15])
            T_e.append(tm)
            tm = float(words[11])
            J_re.append(tm)
    plt.close()
    plt.figure(num=None, figsize=(48, 16), dpi=180, facecolor='w', edgecolor='k')
    plt.rcParams.update({'font.size': 72})
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.title(r'\textbf{Current quench}')
    plt.ylabel(r'\textbf{Normalized}')
    plt.xlabel(r'\textbf{Time (e-e collision time)} ')
    #ticks = np.arange(.01, .1, 2)
    #ticklabels = [r"$10^{}$".format(tick) for tick in ticks]
    #plt.yticks(ticks, ticklabels)
    plt.plot(time, np.divide(n_e,n_e[0]),'b*-', label=r'\textbf{charge density ($n_e$)}')
    plt.plot(time, np.divide(E,E[0]),    'rx-.',label=r'\textbf{electric field (E)}')
    plt.plot(time, np.divide(T_e,T_e[0]),'go--',label=r'\textbf{temperature ($T_e$)}')
    plt.plot(time, np.divide(J,J[Jidx]), 'kd:',label=r'\textbf{current (J)}')
#    plt.plot(time, np.divide(J_re,J[Jidx]), 'ms-.',label=r'\textbf{Runaway J ($J_re$)}')
    print ('Jidx=',Jidx)
#    plt.legend(bbox_to_anchor=(.3, 1.), loc='upper right', shadow=False, fancybox=True, prop={'size': 36})
    plt.legend(loc='upper right', shadow=False, fancybox=True, prop={'size': 48})
#    plt.legend(loc='upper left', title=r"\textbf{Plasma quantites}", shadow=False, fancybox=True)
#    plt.tight_layout()
    #ax1.set_aspect(aspect=2)
    xmin, xmax, ymin, ymax = plt.axis()
    yymax = 15
    if ymax > yymax : ymax = yymax
    xmin, xmax, ymin, ymax = plt.axis([0, 140, -.1, ymax ])
    plt.grid()
    plt.savefig(solver_type + '_quench_profiles.png')
#plt.show()
