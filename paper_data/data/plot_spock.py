#!/usr/bin/env python3
# For Cori
# Usage: plot_tput out11_NSMP_NC_Kokkos.txt
#                  0     1    2  
#
#import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd
solve_time = np.zeros( (1, 2, 4) )
newt_sec = np.zeros(   (1, 2, 4) )
time_1_1 = np.zeros( (6, 1) )

np_idx_arr = [-1, 0,  1, -1, 2,  -1,  -1,  -1, 3]
user_args = sys.argv[1:]
nr = 1
nfiles = 0
for filename in sys.argv[1:]:
    print (filename)
    nfiles += 1
    base = filename.split('.')
    parts = base[0].split('_')
    smp = int(parts[1])
    nc = int(parts[2])
    be_idx = 0
    np = smp*nc
    np_idx = np_idx_arr[nc]
    smp_idx = smp - 1
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if n > 1 and words[1] == 'Solve:':
            tm = float(words[2])
            solve_time[be_idx][smp_idx][np_idx] = tm
            if (smp==1 and nc==1):
                time_1_1[0][be_idx] = tm # total time
#            print (base,smp,nc,tm)
        elif n > 2 and words[0] == 'Landau' and words[1] == 'Jacobian':
            nn = float(words[2])
            newt_sec[be_idx][smp_idx][np_idx] = (nr*np*nn)/(solve_time[be_idx][smp_idx][np_idx])
        elif n > 2 and words[0] == 'Landau' and words[1] == 'Operator':
            tm = float(words[4])
            if (smp==1 and nc==1):
                time_1_1[1][be_idx] = tm
        elif n > 2 and words[0] == 'Kernel' and int(words[2]) != 29:
            tm = float(words[4])
            if (smp==1 and nc==1):
                time_1_1[2][be_idx] = tm
        elif n > 2 and words[0] == 'MatLUFactorNum':
            tm = float(words[3])
            if (smp==1 and nc==1):
                time_1_1[3][be_idx] = tm
        elif n > 2 and words[0] == 'Jac-f-df':
            try:             
                tm = float(words[4])
                if (smp==1 and nc==1):
                    time_1_1[5][be_idx] = tm
            except ValueError:
                print(ValueError)
        elif n > 2 and words[0] == 'MatSolve':
            tm = float(words[3])
            if (smp==1 and nc==1):
                time_1_1[4][be_idx] = tm
if nfiles == 0: print('no files: Usage plot_spock.py out*')
#            print ('       np=', np, ', num=', nn, ' num tot = ', np*nn, ' time=',solve_time[be_idx][smp_idx][np_idx],' rate=', newt_sec[be_idx][smp_idx][np_idx])
pd.options.display.float_format = '{:,}'.format
#
smp_names =  ['1', '2']
np_names =   ['1','2','4','8']
#
dfc = pd.DataFrame(newt_sec[0][:][:], index=smp_names, columns=np_names)
dfc.index.name = 'process/core'
dfc.columns.name = 'cores/GPU'
print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='HIP, CPU LU, MI100 Newton iterations / sec', label='tab:hip'))

arch_names = ['Kokkos-HIP' ]
part_names = ['Total', 'Landau', '(Kernel)', 'factor', 'solve']

time_1_1[2][:] = time_1_1[2][:] + time_1_1[:][-1] # Add f,df/dx GPU time to "Kernel"
dfp = pd.DataFrame(time_1_1[:][:-1].T, index=arch_names, columns=part_names)
dfp.columns.name = 'Device'
#print (dfp)
#dfp.index.name = 'pc'
#dfp.columns.name = 'Newton iterations per second'
print (dfp.to_latex(longtable=False, escape=False, float_format="{:,.1f}".format, caption='Component times -- AMD MI100', label='tab:parts11'))
