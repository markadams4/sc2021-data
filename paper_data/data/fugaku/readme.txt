As of 20 April 2021 the data was generated with these compilier environments:`

/vol0004/apps/oss/spack/share/spack/setup-env.sh
spack load gcc@10.2.0%gcc@8.3.1 arch=linux-rhel8-a64fx
spack load fujitsu-mpi@4.5.0%gcc@8.3.1 arch=linux-rhel8-a64fx

CC=mpicc
CXX=mpiCC
FC=mpif90
COPTFLAGS=-Ofast -march=armv8.2-a+sve -msve-vector-bits=512
CXXOPTFLAGS=-Ofast -march=armv8.2-a+sve -msve-vector-bits=512

