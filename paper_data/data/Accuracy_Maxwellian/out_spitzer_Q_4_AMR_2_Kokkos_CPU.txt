masses:        e= 9.109e-31; ions in proton mass units:    2.000e+00  0.000e+00 ...
charges:       e=-1.602e-19; charges in elementary units:  1.000e+00  0.000e+00
thermal T (K): e= 1.160e+07 i= 1.160e+07  0.000e+00. v_0= 1.326e+07 ( 4.424e-02c) n_0= 1.000e+20 t_0= 5.787e-06, classical, Intuitive
Domain radius grid 0:  5.000e+00, 1:  8.252e-02
CalculateE j0=145647. Ec = 0.0509908
0 TS dt 0.5 time 0.
  0) species-0: charge density= -1.6021381694503e+01 z-momentum=  1.6673947178029e-19 energy=  2.4032171924474e+04
  0) species-1: charge density=  1.6021381694503e+01 z-momentum=  5.9427000052529e-18 energy=  2.4032171924474e+04
	  0) Total: charge density=  0.0000000000000e+00, momentum=  6.1094394770332e-18, energy=  4.8064343848948e+04 (m_i[0]/m_e = 3670.94, 14 cells)
testSpitzer J =  2.120e-08
testSpitzer:    0) time= 0.0000e+00 n_e=  1.000e+00 E=  2.550e-03 J=  2.120e-08 J_re=  4.147e-09 19.6% Te_kev=  1.000e+00 Z_eff=1. E/J to eta ratio= 7.19146e+12 (diff=-7.18146e+12) constant E normal
0) FormLandau: 700 IPs, 14 cells[0], Nb=25, Nq=25, dim=2, Tab: Nb=25 Nf=2 Np=25 cdim=2 N=490
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   0 accepted t=0          + 5.000e-01 dt=5.500e-01  wlte=0.332  wltea=   -1 wlter=   -1
1 TS dt 0.55 time 0.5
testSpitzer J =  1.810e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   1 accepted t=0.5        + 5.500e-01 dt=6.050e-01  wlte= 0.23  wltea=   -1 wlter=   -1
2 TS dt 0.605 time 1.05
testSpitzer J =  3.487e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   2 accepted t=1.05       + 6.050e-01 dt=6.655e-01  wlte=0.157  wltea=   -1 wlter=   -1
3 TS dt 0.6655 time 1.655
testSpitzer J =  5.045e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   3 accepted t=1.655      + 6.655e-01 dt=7.321e-01  wlte= 0.15  wltea=   -1 wlter=   -1
4 TS dt 0.73205 time 2.3205
testSpitzer J =  6.485e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   4 accepted t=2.3205     + 7.321e-01 dt=8.053e-01  wlte=0.262  wltea=   -1 wlter=   -1
5 TS dt 0.805255 time 3.05255
testSpitzer J =  7.809e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   5 accepted t=3.05255    + 8.053e-01 dt=8.858e-01  wlte= 0.19  wltea=   -1 wlter=   -1
6 TS dt 0.885781 time 3.85781
testSpitzer J =  9.015e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   6 accepted t=3.85781    + 8.858e-01 dt=9.744e-01  wlte=0.112  wltea=   -1 wlter=   -1
7 TS dt 0.974359 time 4.74359
testSpitzer J =  1.010e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   7 accepted t=4.74359    + 9.744e-01 dt=1.000e+00  wlte=0.118  wltea=   -1 wlter=   -1
8 TS dt 1. time 5.71794
testSpitzer J =  1.107e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step   8 accepted t=5.71794    + 1.000e+00 dt=1.000e+00  wlte=0.069  wltea=   -1 wlter=   -1
9 TS dt 1. time 6.71794
testSpitzer J =  1.187e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step   9 accepted t=6.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0546  wltea=   -1 wlter=   -1
10 TS dt 1. time 7.71794
testSpitzer J =  1.252e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  10 accepted t=7.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0449  wltea=   -1 wlter=   -1
11 TS dt 1. time 8.71794
testSpitzer J =  1.304e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  11 accepted t=8.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0376  wltea=   -1 wlter=   -1
12 TS dt 1. time 9.71794
testSpitzer J =  1.347e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  12 accepted t=9.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0318  wltea=   -1 wlter=   -1
13 TS dt 1. time 10.7179
testSpitzer J =  1.383e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  13 accepted t=10.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0271  wltea=   -1 wlter=   -1
14 TS dt 1. time 11.7179
testSpitzer J =  1.411e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  14 accepted t=11.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0232  wltea=   -1 wlter=   -1
15 TS dt 1. time 12.7179
testSpitzer J =  1.435e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  15 accepted t=12.7179    + 1.000e+00 dt=1.000e+00  wlte= 0.02  wltea=   -1 wlter=   -1
16 TS dt 1. time 13.7179
testSpitzer J =  1.455e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  16 accepted t=13.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0173  wltea=   -1 wlter=   -1
17 TS dt 1. time 14.7179
testSpitzer J =  1.471e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  17 accepted t=14.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0151  wltea=   -1 wlter=   -1
18 TS dt 1. time 15.7179
testSpitzer J =  1.484e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  18 accepted t=15.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0134  wltea=   -1 wlter=   -1
19 TS dt 1. time 16.7179
testSpitzer J =  1.495e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  19 accepted t=16.7179    + 1.000e+00 dt=1.000e+00  wlte=0.012  wltea=   -1 wlter=   -1
20 TS dt 1. time 17.7179
testSpitzer J =  1.504e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  20 accepted t=17.7179    + 1.000e+00 dt=1.000e+00  wlte=0.011  wltea=   -1 wlter=   -1
21 TS dt 1. time 18.7179
testSpitzer J =  1.512e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  21 accepted t=18.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0103  wltea=   -1 wlter=   -1
22 TS dt 1. time 19.7179
testSpitzer J =  1.518e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  22 accepted t=19.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00996  wltea=   -1 wlter=   -1
23 TS dt 1. time 20.7179
testSpitzer J =  1.523e+05
testSpitzer:   23) time= 2.0718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.523e+05 J_re=  1.440e+04 9.45% Te_kev=  1.001e+00 Z_eff=1. E/J to eta ratio= 1.00286 (diff=0.00339508) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  23 accepted t=20.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00973  wltea=   -1 wlter=   -1
24 TS dt 1. time 21.7179
testSpitzer J =  1.528e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  24 accepted t=21.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00943  wltea=   -1 wlter=   -1
25 TS dt 1. time 22.7179
testSpitzer J =  1.531e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  25 accepted t=22.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00914  wltea=   -1 wlter=   -1
26 TS dt 1. time 23.7179
testSpitzer J =  1.534e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  26 accepted t=23.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00886  wltea=   -1 wlter=   -1
27 TS dt 1. time 24.7179
testSpitzer J =  1.537e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  27 accepted t=24.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00857  wltea=   -1 wlter=   -1
28 TS dt 1. time 25.7179
testSpitzer J =  1.539e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  28 accepted t=25.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00828  wltea=   -1 wlter=   -1
29 TS dt 1. time 26.7179
testSpitzer J =  1.540e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  29 accepted t=26.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00798  wltea=   -1 wlter=   -1
30 TS dt 1. time 27.7179
testSpitzer J =  1.542e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  30 accepted t=27.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00768  wltea=   -1 wlter=   -1
31 TS dt 1. time 28.7179
testSpitzer J =  1.543e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  31 accepted t=28.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00738  wltea=   -1 wlter=   -1
32 TS dt 1. time 29.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  32 accepted t=29.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00707  wltea=   -1 wlter=   -1
33 TS dt 1. time 30.7179
testSpitzer J =  1.545e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  33 accepted t=30.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00676  wltea=   -1 wlter=   -1
34 TS dt 1. time 31.7179
testSpitzer J =  1.546e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  34 accepted t=31.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00644  wltea=   -1 wlter=   -1
35 TS dt 1. time 32.7179
testSpitzer J =  1.546e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  35 accepted t=32.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00612  wltea=   -1 wlter=   -1
36 TS dt 1. time 33.7179
testSpitzer J =  1.547e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  36 accepted t=33.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00581  wltea=   -1 wlter=   -1
37 TS dt 1. time 34.7179
testSpitzer J =  1.547e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  37 accepted t=34.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00551  wltea=   -1 wlter=   -1
38 TS dt 1. time 35.7179
testSpitzer J =  1.547e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  38 accepted t=35.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00522  wltea=   -1 wlter=   -1
39 TS dt 1. time 36.7179
testSpitzer J =  1.548e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  39 accepted t=36.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00494  wltea=   -1 wlter=   -1
40 TS dt 1. time 37.7179
testSpitzer J =  1.548e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  40 accepted t=37.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00467  wltea=   -1 wlter=   -1
41 TS dt 1. time 38.7179
testSpitzer J =  1.548e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  41 accepted t=38.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00441  wltea=   -1 wlter=   -1
42 TS dt 1. time 39.7179
testSpitzer J =  1.548e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  42 accepted t=39.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00416  wltea=   -1 wlter=   -1
43 TS dt 1. time 40.7179
testSpitzer J =  1.548e+05
testSpitzer:   43) time= 4.0718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.548e+05 J_re=  1.481e+04 9.56% Te_kev=  1.002e+00 Z_eff=1. E/J to eta ratio= 0.986861 (diff=8.93823e-05) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  43 accepted t=40.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00392  wltea=   -1 wlter=   -1
44 TS dt 1. time 41.7179
testSpitzer J =  1.548e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  44 accepted t=41.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00369  wltea=   -1 wlter=   -1
45 TS dt 1. time 42.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  45 accepted t=42.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00347  wltea=   -1 wlter=   -1
46 TS dt 1. time 43.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  46 accepted t=43.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00326  wltea=   -1 wlter=   -1
47 TS dt 1. time 44.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  47 accepted t=44.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00307  wltea=   -1 wlter=   -1
48 TS dt 1. time 45.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  48 accepted t=45.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00288  wltea=   -1 wlter=   -1
49 TS dt 1. time 46.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  49 accepted t=46.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0027  wltea=   -1 wlter=   -1
50 TS dt 1. time 47.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  50 accepted t=47.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00253  wltea=   -1 wlter=   -1
51 TS dt 1. time 48.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  51 accepted t=48.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00236  wltea=   -1 wlter=   -1
52 TS dt 1. time 49.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  52 accepted t=49.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00221  wltea=   -1 wlter=   -1
53 TS dt 1. time 50.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  53 accepted t=50.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00207  wltea=   -1 wlter=   -1
54 TS dt 1. time 51.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  54 accepted t=51.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00193  wltea=   -1 wlter=   -1
55 TS dt 1. time 52.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  55 accepted t=52.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0018  wltea=   -1 wlter=   -1
56 TS dt 1. time 53.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  56 accepted t=53.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00168  wltea=   -1 wlter=   -1
57 TS dt 1. time 54.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  57 accepted t=54.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00157  wltea=   -1 wlter=   -1
58 TS dt 1. time 55.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  58 accepted t=55.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00146  wltea=   -1 wlter=   -1
59 TS dt 1. time 56.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  59 accepted t=56.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00136  wltea=   -1 wlter=   -1
60 TS dt 1. time 57.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  60 accepted t=57.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00127  wltea=   -1 wlter=   -1
61 TS dt 1. time 58.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  61 accepted t=58.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00119  wltea=   -1 wlter=   -1
62 TS dt 1. time 59.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  62 accepted t=59.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00112  wltea=   -1 wlter=   -1
63 TS dt 1. time 60.7179
testSpitzer J =  1.549e+05
testSpitzer:   63) time= 6.0718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.549e+05 J_re=  1.484e+04 9.58% Te_kev=  1.002e+00 Z_eff=1. E/J to eta ratio= 0.986324 (diff=6.6205e-06) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  63 accepted t=60.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00106  wltea=   -1 wlter=   -1
64 TS dt 1. time 61.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  64 accepted t=61.7179    + 1.000e+00 dt=1.000e+00  wlte=0.001  wltea=   -1 wlter=   -1
65 TS dt 1. time 62.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  65 accepted t=62.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000961  wltea=   -1 wlter=   -1
66 TS dt 1. time 63.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  66 accepted t=63.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000926  wltea=   -1 wlter=   -1
67 TS dt 1. time 64.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  67 accepted t=64.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000897  wltea=   -1 wlter=   -1
68 TS dt 1. time 65.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  68 accepted t=65.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000874  wltea=   -1 wlter=   -1
69 TS dt 1. time 66.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  69 accepted t=66.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000855  wltea=   -1 wlter=   -1
70 TS dt 1. time 67.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  70 accepted t=67.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000839  wltea=   -1 wlter=   -1
71 TS dt 1. time 68.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  71 accepted t=68.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000824  wltea=   -1 wlter=   -1
72 TS dt 1. time 69.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  72 accepted t=69.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000811  wltea=   -1 wlter=   -1
73 TS dt 1. time 70.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  73 accepted t=70.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000799  wltea=   -1 wlter=   -1
74 TS dt 1. time 71.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  74 accepted t=71.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000787  wltea=   -1 wlter=   -1
75 TS dt 1. time 72.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  75 accepted t=72.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000776  wltea=   -1 wlter=   -1
76 TS dt 1. time 73.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  76 accepted t=73.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000764  wltea=   -1 wlter=   -1
77 TS dt 1. time 74.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  77 accepted t=74.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000753  wltea=   -1 wlter=   -1
78 TS dt 1. time 75.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  78 accepted t=75.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000741  wltea=   -1 wlter=   -1
79 TS dt 1. time 76.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  79 accepted t=76.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000729  wltea=   -1 wlter=   -1
80 TS dt 1. time 77.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  80 accepted t=77.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000717  wltea=   -1 wlter=   -1
81 TS dt 1. time 78.7179
testSpitzer J =  1.549e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  81 accepted t=78.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000705  wltea=   -1 wlter=   -1
82 TS dt 1. time 79.7179
testSpitzer J =  1.549e+05
testSpitzer:   82) time= 7.9718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.549e+05 J_re=  1.484e+04 9.58% Te_kev=  1.002e+00 Z_eff=1. E/J to eta ratio= 0.986272 (diff=8.98721e-07) using Spitzer eta*J E transition
