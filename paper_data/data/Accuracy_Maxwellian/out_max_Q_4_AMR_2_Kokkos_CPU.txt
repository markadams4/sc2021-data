masses:        e= 9.109e-31; ions in proton mass units:    2.000e+00  0.000e+00 ...
charges:       e=-1.602e-19; charges in elementary units:  1.000e+00  0.000e+00
thermal T (K): e= 1.160e+07 i= 1.160e+07  0.000e+00. v_0= 1.326e+07 ( 4.424e-02c) n_0= 1.000e+20 t_0= 5.787e-06, classical, Intuitive
Domain radius grid 0:  5.000e+00, 1:  8.252e-02
CalculateE j0=0. Ec = 0.0509908
0 TS dt 0.5 time 0.
  0) species-0: charge density= -1.6021381694503e+01 z-momentum=  1.6673947178029e-19 energy=  2.4032171924474e+04
  0) species-1: charge density=  1.6021381694503e+01 z-momentum=  5.9427000052529e-18 energy=  2.4032171924474e+04
	  0) Total: charge density=  0.0000000000000e+00, momentum=  6.1094394770332e-18, energy=  4.8064343848948e+04 (m_i[0]/m_e = 3670.94, 14 cells)
[0] parallel consistency check OK
