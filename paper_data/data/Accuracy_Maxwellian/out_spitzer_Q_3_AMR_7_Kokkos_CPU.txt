masses:        e= 9.109e-31; ions in proton mass units:    2.000e+00  0.000e+00 ...
charges:       e=-1.602e-19; charges in elementary units:  1.000e+00  0.000e+00
thermal T (K): e= 1.160e+07 i= 1.160e+07  0.000e+00. v_0= 1.326e+07 ( 4.424e-02c) n_0= 1.000e+20 t_0= 5.787e-06, classical, Intuitive
Domain radius grid 0:  5.000e+00, 1:  8.252e-02
CalculateE j0=145647. Ec = 0.0509908
0 TS dt 0.5 time 0.
  0) species-0: charge density= -1.6021931103749e+01 z-momentum=  1.1125913553203e-19 energy=  2.4031686610989e+04
  0) species-1: charge density=  1.6021931103749e+01 z-momentum= -1.7883209276410e-18 energy=  2.4031686610989e+04
	  0) Total: charge density= -7.1054273576010e-15, momentum= -1.6770617921090e-18, energy=  4.8063373221978e+04 (m_i[0]/m_e = 3670.94, 1024 cells)
testSpitzer J =  3.005e-08
testSpitzer:    0) time= 0.0000e+00 n_e=  1.000e+00 E=  2.550e-03 J=  3.005e-08 J_re= -6.975e-11 -0.232% Te_kev=  1.000e+00 Z_eff=1. E/J to eta ratio= 5.0724e+12 (diff=-5.0624e+12) constant E normal
0) FormLandau: 32768 IPs, 1024 cells[0], Nb=16, Nq=16, dim=2, Tab: Nb=16 Nf=2 Np=16 cdim=2 N=18818
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   0 accepted t=0          + 5.000e-01 dt=5.500e-01  wlte=0.000111  wltea=   -1 wlter=   -1
1 TS dt 0.55 time 0.5
testSpitzer J =  1.810e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   1 accepted t=0.5        + 5.500e-01 dt=6.050e-01  wlte=9.36e-05  wltea=   -1 wlter=   -1
2 TS dt 0.605 time 1.05
testSpitzer J =  3.488e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   2 accepted t=1.05       + 6.050e-01 dt=6.655e-01  wlte=8.08e-05  wltea=   -1 wlter=   -1
3 TS dt 0.6655 time 1.655
testSpitzer J =  5.044e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   3 accepted t=1.655      + 6.655e-01 dt=7.321e-01  wlte=7.02e-05  wltea=   -1 wlter=   -1
4 TS dt 0.73205 time 2.3205
testSpitzer J =  6.485e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   4 accepted t=2.3205     + 7.321e-01 dt=8.053e-01  wlte=6.09e-05  wltea=   -1 wlter=   -1
5 TS dt 0.805255 time 3.05255
testSpitzer J =  7.808e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   5 accepted t=3.05255    + 8.053e-01 dt=8.858e-01  wlte=5.29e-05  wltea=   -1 wlter=   -1
6 TS dt 0.885781 time 3.85781
testSpitzer J =  9.013e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   6 accepted t=3.85781    + 8.858e-01 dt=9.744e-01  wlte=4.57e-05  wltea=   -1 wlter=   -1
7 TS dt 0.974359 time 4.74359
testSpitzer J =  1.010e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   7 accepted t=4.74359    + 9.744e-01 dt=1.000e+00  wlte=3.96e-05  wltea=   -1 wlter=   -1
8 TS dt 1. time 5.71794
testSpitzer J =  1.107e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   8 accepted t=5.71794    + 1.000e+00 dt=1.000e+00  wlte=3.11e-05  wltea=   -1 wlter=   -1
9 TS dt 1. time 6.71794
testSpitzer J =  1.186e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step   9 accepted t=6.71794    + 1.000e+00 dt=1.000e+00  wlte=2.38e-05  wltea=   -1 wlter=   -1
10 TS dt 1. time 7.71794
testSpitzer J =  1.251e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  10 accepted t=7.71794    + 1.000e+00 dt=1.000e+00  wlte=1.86e-05  wltea=   -1 wlter=   -1
11 TS dt 1. time 8.71794
testSpitzer J =  1.303e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  11 accepted t=8.71794    + 1.000e+00 dt=1.000e+00  wlte=1.47e-05  wltea=   -1 wlter=   -1
12 TS dt 1. time 9.71794
testSpitzer J =  1.346e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  12 accepted t=9.71794    + 1.000e+00 dt=1.000e+00  wlte=1.18e-05  wltea=   -1 wlter=   -1
13 TS dt 1. time 10.7179
testSpitzer J =  1.381e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  13 accepted t=10.7179    + 1.000e+00 dt=1.000e+00  wlte=9.71e-06  wltea=   -1 wlter=   -1
14 TS dt 1. time 11.7179
testSpitzer J =  1.409e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  14 accepted t=11.7179    + 1.000e+00 dt=1.000e+00  wlte=7.83e-06  wltea=   -1 wlter=   -1
15 TS dt 1. time 12.7179
testSpitzer J =  1.433e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  15 accepted t=12.7179    + 1.000e+00 dt=1.000e+00  wlte=6.57e-06  wltea=   -1 wlter=   -1
16 TS dt 1. time 13.7179
testSpitzer J =  1.452e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  16 accepted t=13.7179    + 1.000e+00 dt=1.000e+00  wlte=5.42e-06  wltea=   -1 wlter=   -1
17 TS dt 1. time 14.7179
testSpitzer J =  1.468e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  17 accepted t=14.7179    + 1.000e+00 dt=1.000e+00  wlte=4.49e-06  wltea=   -1 wlter=   -1
18 TS dt 1. time 15.7179
testSpitzer J =  1.481e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  18 accepted t=15.7179    + 1.000e+00 dt=1.000e+00  wlte=3.73e-06  wltea=   -1 wlter=   -1
19 TS dt 1. time 16.7179
testSpitzer J =  1.491e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  19 accepted t=16.7179    + 1.000e+00 dt=1.000e+00  wlte=3.1e-06  wltea=   -1 wlter=   -1
20 TS dt 1. time 17.7179
testSpitzer J =  1.500e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  20 accepted t=17.7179    + 1.000e+00 dt=1.000e+00  wlte=2.59e-06  wltea=   -1 wlter=   -1
21 TS dt 1. time 18.7179
testSpitzer J =  1.508e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  21 accepted t=18.7179    + 1.000e+00 dt=1.000e+00  wlte=2.16e-06  wltea=   -1 wlter=   -1
22 TS dt 1. time 19.7179
testSpitzer J =  1.514e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
      TSAdapt basic arkimex 0:1bee step  22 accepted t=19.7179    + 1.000e+00 dt=1.000e+00  wlte=2.08e-06  wltea=   -1 wlter=   -1
23 TS dt 1. time 20.7179
testSpitzer J =  1.519e+05
testSpitzer:   23) time= 2.0718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.519e+05 J_re=  9.894e+03 6.51% Te_kev=  1.000e+00 Z_eff=1. E/J to eta ratio= 1.00365 (diff=0.003352) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  23 accepted t=20.7179    + 1.000e+00 dt=1.000e+00  wlte=1.6e-06  wltea=   -1 wlter=   -1
24 TS dt 1. time 21.7179
testSpitzer J =  1.523e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  24 accepted t=21.7179    + 1.000e+00 dt=1.000e+00  wlte=1.34e-06  wltea=   -1 wlter=   -1
25 TS dt 1. time 22.7179
testSpitzer J =  1.526e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  25 accepted t=22.7179    + 1.000e+00 dt=1.000e+00  wlte=1.13e-06  wltea=   -1 wlter=   -1
26 TS dt 1. time 23.7179
testSpitzer J =  1.529e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  26 accepted t=23.7179    + 1.000e+00 dt=1.000e+00  wlte=9.56e-07  wltea=   -1 wlter=   -1
27 TS dt 1. time 24.7179
testSpitzer J =  1.532e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  27 accepted t=24.7179    + 1.000e+00 dt=1.000e+00  wlte=8.11e-07  wltea=   -1 wlter=   -1
28 TS dt 1. time 25.7179
testSpitzer J =  1.534e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 3
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  28 accepted t=25.7179    + 1.000e+00 dt=1.000e+00  wlte=6.9e-07  wltea=   -1 wlter=   -1
29 TS dt 1. time 26.7179
testSpitzer J =  1.535e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  29 accepted t=26.7179    + 1.000e+00 dt=1.000e+00  wlte=8.19e-07  wltea=   -1 wlter=   -1
30 TS dt 1. time 27.7179
testSpitzer J =  1.537e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  30 accepted t=27.7179    + 1.000e+00 dt=1.000e+00  wlte=7.03e-07  wltea=   -1 wlter=   -1
31 TS dt 1. time 28.7179
testSpitzer J =  1.538e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  31 accepted t=28.7179    + 1.000e+00 dt=1.000e+00  wlte=6.09e-07  wltea=   -1 wlter=   -1
32 TS dt 1. time 29.7179
testSpitzer J =  1.539e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  32 accepted t=29.7179    + 1.000e+00 dt=1.000e+00  wlte=5.32e-07  wltea=   -1 wlter=   -1
33 TS dt 1. time 30.7179
testSpitzer J =  1.540e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  33 accepted t=30.7179    + 1.000e+00 dt=1.000e+00  wlte=4.69e-07  wltea=   -1 wlter=   -1
34 TS dt 1. time 31.7179
testSpitzer J =  1.540e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  34 accepted t=31.7179    + 1.000e+00 dt=1.000e+00  wlte=4.19e-07  wltea=   -1 wlter=   -1
35 TS dt 1. time 32.7179
testSpitzer J =  1.541e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  35 accepted t=32.7179    + 1.000e+00 dt=1.000e+00  wlte=3.79e-07  wltea=   -1 wlter=   -1
36 TS dt 1. time 33.7179
testSpitzer J =  1.541e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  36 accepted t=33.7179    + 1.000e+00 dt=1.000e+00  wlte=3.47e-07  wltea=   -1 wlter=   -1
37 TS dt 1. time 34.7179
testSpitzer J =  1.542e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  37 accepted t=34.7179    + 1.000e+00 dt=1.000e+00  wlte=3.22e-07  wltea=   -1 wlter=   -1
38 TS dt 1. time 35.7179
testSpitzer J =  1.542e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  38 accepted t=35.7179    + 1.000e+00 dt=1.000e+00  wlte=3.01e-07  wltea=   -1 wlter=   -1
39 TS dt 1. time 36.7179
testSpitzer J =  1.542e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  39 accepted t=36.7179    + 1.000e+00 dt=1.000e+00  wlte=2.86e-07  wltea=   -1 wlter=   -1
40 TS dt 1. time 37.7179
testSpitzer J =  1.543e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  40 accepted t=37.7179    + 1.000e+00 dt=1.000e+00  wlte=2.73e-07  wltea=   -1 wlter=   -1
41 TS dt 1. time 38.7179
testSpitzer J =  1.543e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  41 accepted t=38.7179    + 1.000e+00 dt=1.000e+00  wlte=2.63e-07  wltea=   -1 wlter=   -1
42 TS dt 1. time 39.7179
testSpitzer J =  1.543e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  42 accepted t=39.7179    + 1.000e+00 dt=1.000e+00  wlte=2.55e-07  wltea=   -1 wlter=   -1
43 TS dt 1. time 40.7179
testSpitzer J =  1.543e+05
testSpitzer:   43) time= 4.0718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.543e+05 J_re=  1.024e+04 6.64% Te_kev=  1.000e+00 Z_eff=1. E/J to eta ratio= 0.987738 (diff=9.09995e-05) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  43 accepted t=40.7179    + 1.000e+00 dt=1.000e+00  wlte=2.49e-07  wltea=   -1 wlter=   -1
44 TS dt 1. time 41.7179
testSpitzer J =  1.543e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  44 accepted t=41.7179    + 1.000e+00 dt=1.000e+00  wlte=2.44e-07  wltea=   -1 wlter=   -1
45 TS dt 1. time 42.7179
testSpitzer J =  1.543e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  45 accepted t=42.7179    + 1.000e+00 dt=1.000e+00  wlte=2.4e-07  wltea=   -1 wlter=   -1
46 TS dt 1. time 43.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  46 accepted t=43.7179    + 1.000e+00 dt=1.000e+00  wlte=2.37e-07  wltea=   -1 wlter=   -1
47 TS dt 1. time 44.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  47 accepted t=44.7179    + 1.000e+00 dt=1.000e+00  wlte=2.34e-07  wltea=   -1 wlter=   -1
48 TS dt 1. time 45.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  48 accepted t=45.7179    + 1.000e+00 dt=1.000e+00  wlte=2.32e-07  wltea=   -1 wlter=   -1
49 TS dt 1. time 46.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  49 accepted t=46.7179    + 1.000e+00 dt=1.000e+00  wlte=2.3e-07  wltea=   -1 wlter=   -1
50 TS dt 1. time 47.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  50 accepted t=47.7179    + 1.000e+00 dt=1.000e+00  wlte=2.28e-07  wltea=   -1 wlter=   -1
51 TS dt 1. time 48.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  51 accepted t=48.7179    + 1.000e+00 dt=1.000e+00  wlte=2.27e-07  wltea=   -1 wlter=   -1
52 TS dt 1. time 49.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  52 accepted t=49.7179    + 1.000e+00 dt=1.000e+00  wlte=2.26e-07  wltea=   -1 wlter=   -1
53 TS dt 1. time 50.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  53 accepted t=50.7179    + 1.000e+00 dt=1.000e+00  wlte=2.25e-07  wltea=   -1 wlter=   -1
54 TS dt 1. time 51.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  54 accepted t=51.7179    + 1.000e+00 dt=1.000e+00  wlte=2.24e-07  wltea=   -1 wlter=   -1
55 TS dt 1. time 52.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  55 accepted t=52.7179    + 1.000e+00 dt=1.000e+00  wlte=2.23e-07  wltea=   -1 wlter=   -1
56 TS dt 1. time 53.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  56 accepted t=53.7179    + 1.000e+00 dt=1.000e+00  wlte=2.22e-07  wltea=   -1 wlter=   -1
57 TS dt 1. time 54.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  57 accepted t=54.7179    + 1.000e+00 dt=1.000e+00  wlte=2.22e-07  wltea=   -1 wlter=   -1
58 TS dt 1. time 55.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  58 accepted t=55.7179    + 1.000e+00 dt=1.000e+00  wlte=2.21e-07  wltea=   -1 wlter=   -1
59 TS dt 1. time 56.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  59 accepted t=56.7179    + 1.000e+00 dt=1.000e+00  wlte=2.21e-07  wltea=   -1 wlter=   -1
60 TS dt 1. time 57.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  60 accepted t=57.7179    + 1.000e+00 dt=1.000e+00  wlte=2.2e-07  wltea=   -1 wlter=   -1
61 TS dt 1. time 58.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  61 accepted t=58.7179    + 1.000e+00 dt=1.000e+00  wlte=2.2e-07  wltea=   -1 wlter=   -1
62 TS dt 1. time 59.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  62 accepted t=59.7179    + 1.000e+00 dt=1.000e+00  wlte=2.2e-07  wltea=   -1 wlter=   -1
63 TS dt 1. time 60.7179
testSpitzer J =  1.544e+05
testSpitzer:   63) time= 6.0718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.544e+05 J_re=  1.025e+04 6.64% Te_kev=  1.000e+00 Z_eff=1. E/J to eta ratio= 0.98726 (diff=3.31081e-06) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  63 accepted t=60.7179    + 1.000e+00 dt=1.000e+00  wlte=2.19e-07  wltea=   -1 wlter=   -1
64 TS dt 1. time 61.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  64 accepted t=61.7179    + 1.000e+00 dt=1.000e+00  wlte=2.19e-07  wltea=   -1 wlter=   -1
65 TS dt 1. time 62.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  65 accepted t=62.7179    + 1.000e+00 dt=1.000e+00  wlte=2.19e-07  wltea=   -1 wlter=   -1
66 TS dt 1. time 63.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  66 accepted t=63.7179    + 1.000e+00 dt=1.000e+00  wlte=2.18e-07  wltea=   -1 wlter=   -1
67 TS dt 1. time 64.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  67 accepted t=64.7179    + 1.000e+00 dt=1.000e+00  wlte=2.18e-07  wltea=   -1 wlter=   -1
68 TS dt 1. time 65.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  68 accepted t=65.7179    + 1.000e+00 dt=1.000e+00  wlte=2.18e-07  wltea=   -1 wlter=   -1
69 TS dt 1. time 66.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  69 accepted t=66.7179    + 1.000e+00 dt=1.000e+00  wlte=2.18e-07  wltea=   -1 wlter=   -1
70 TS dt 1. time 67.7179
testSpitzer J =  1.544e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 2
      TSAdapt basic arkimex 0:1bee step  70 accepted t=67.7179    + 1.000e+00 dt=1.000e+00  wlte=2.18e-07  wltea=   -1 wlter=   -1
71 TS dt 1. time 68.7179
testSpitzer J =  1.544e+05
testSpitzer:   71) time= 6.8718e+01 n_e=  1.000e+00 E=  2.550e-03 J=  1.544e+05 J_re=  1.025e+04 6.64% Te_kev=  1.000e+00 Z_eff=1. E/J to eta ratio= 0.987246 (diff=9.34924e-07) using Spitzer eta*J E transition
