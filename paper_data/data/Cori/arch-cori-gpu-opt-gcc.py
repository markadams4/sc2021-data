#!/usr/bin/python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-mpi-dir=/usr/common/software/sles15_cgpu/openmpi/4.0.3/gcc',
    '--with-cuda-dir=/usr/common/software/sles15_cgpu/cuda/11.1.1',
    '--CFLAGS=   -g -DLANDAU_DIM=2 -DLANDAU_MAX_SPECIES=10 -DLANDAU_MAX_Q=4',
    '--CXXFLAGS= -g -DLANDAU_DIM=2 -DLANDAU_MAX_SPECIES=10 -DLANDAU_MAX_Q=4',
    '--CUDAFLAGS=-g -Xcompiler -rdynamic -DLANDAU_DIM=2 -DLANDAU_MAX_SPECIES=10 -DLANDAU_MAX_Q=4',    
    '--FFLAGS=   -g ',
    '--COPTFLAGS=   -O3',
    '--CXXOPTFLAGS= -O3',
    '--FOPTFLAGS=   -O3',
    '--download-fblaslapack=1',
    '--with-debugging=0',
    '--with-mpiexec=srun -G 1',
    '--with-cuda-gencodearch=70',
    '--with-batch=0',
    '--with-cuda=1',
    '--download-p4est=1',
    '--with-zlib=1',
    'PETSC_ARCH=arch-cori-gpu-opt-gcc'
  ]
  configure.petsc_configure(configure_options)
